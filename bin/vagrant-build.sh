#!/usr/bin/env bash
#
#
dir="$( cd "$( dirname "$0" )" && pwd )"
base=`dirname $dir`

echo "Dir is: $dir, Base is: $base"
cd "$base"
# clean temp build dir
if [ ! -d build ]; then
	mkdir -p "$base/build"
else
	rm -f build/*
fi

# move required resources into public
mkdir -p apps/www/public/fonts
cp -rf bower_components/font-awesome-stylus/fonts/* apps/www/public/fonts/
cp -f "$base/apps/www/resources/fonts.css" "$base/apps/www/public/css/"
cp -f "$base/bower_components/HTML5-Reset/assets/css/reset.css" "$base/apps/www/public/css/"
# compile stylus into css
stylus "$base/apps/www/resources/styles/default.styl" -o "$base/apps/www/public/css/"
# minimize css
sqwish "$base/apps/www/public/css/default.css" --strict

# compile all pug templates into javascript functions
#pug --client --no-debug apps/www/resources/shared/pug/*.pug -o build/
#puglatizer  -d apps/www/resources/pug/ -o build/templates.js

browserify "$base/apps/www/resources/js/default.js" -o "$base/apps/www/public/js/default.bundle.js"

#rm build/*

