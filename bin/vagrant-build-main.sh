#!/usr/bin/env bash
#
#
dir="$( cd "$( dirname "$0" )" && pwd )"
base=`dirname $dir`

echo "Dir is: $dir, Base is: $base"
cd "$base"
# clean temp build dir
if [ ! -d build ]; then
	mkdir -p "$base/build"
else
	rm -f build/*
fi

# move required resources into public
mkdir -p apps/admin/public/fonts
cp -rf bower_components/font-awesome-stylus/fonts/* apps/admin/public/fonts/
cp -f "$base/apps/admin/resources/shared/styles/fonts.css" "$base/apps/admin/public/css/"
cp -f "$base/bower_components/HTML5-Reset/assets/css/reset.css" "$base/apps/admin/public/css/"
# compile stylus into css
stylus "$base/apps/admin/resources/main/styles/main.styl" -o "$base/apps/admin/public/css/"
# minimize css
sqwish "$base/apps/admin/public/css/main.css" --strict

# compile all pug templates into javascript functions
#pug --client --no-debug apps/admin/resources/shared/pug/*.pug -o build/
puglatizer  -d apps/admin/resources/shared/pug/ -o build/templates.js
puglatizer  -d apps/admin/resources/main/views/ -o build/templates.main.js

browserify "$base/apps/admin/resources/main/js/main.js" -o "$base/apps/admin/public/js/main.bundle.js"

#rm build/*

