<?php
use Phalcon\Di\FactoryDefault;

// configuration values
$config = include __DIR__ . '/../config/config.php';

// credentials
$auth = include __DIR__ . '/../config/auth.php';

$di = new FactoryDefault();

// handles loading
include __DIR__ . '/../config/loader.php';

// ioc services
include __DIR__ . '/../config/services.php';

$application = new \Phalcon\Mvc\Micro($di);

$pages = new \Phalcon\Mvc\Micro\Collection();
$pages->setHandler(new \supervillainhq\andkrupdk\www\cms\routing\CmsController($di));
$pages->get("/{folder:[a-zA-Z0-9\\-_]+}/{pageGuid:[a-zA-Z0-9\\-_]+}", "folderPage");
$pages->get("/{pageGuid:[a-zA-Z0-9\\-_]+}", "page");
$pages->get("/", "index");


$application->setEventsManager($di->getApplicationEventsManager());
$application->mount($pages);
$application->handle();
