<?php
/**
 * loader.php.
 *
 */
$loader = new \Phalcon\Loader();

$loader->registerNamespaces([
	'supervillainhq\andkrupdk\www' => "{$config->application->paths->base}/src/supervillainhq/andkrupdk/www",
	'supervillainhq\andkrupdk\mongo' => "{$config->application->paths->base}/src/supervillainhq/andkrupdk/mongo",
]);
$loader->register();

require_once __DIR__ . '/../../../vendor/autoload.php';
