<?php
/**
 * services.php.
 */
use Phalcon\Events\Manager;
use Phalcon\Mvc\View;
use Phalcon\Db\Adapter\Pdo\Mysql;
use supervillainhq\andkrupdk\www\cms\PageEngine;
use supervillainhq\andkrupdk\www\cms\CmsExceptionHandler;
use supervillainhq\andkrupdk\www\cms\routing\CmsRouteHandler;

$di->set('config', $config);

$di->set('page', function($guid) use ($config){
	return PageEngine::create($guid, $config);
});

$di->set('view', function () use ($config) {

    $view = new View();

//    $view->setViewsDir($config->application->paths->viewsDir);

    $view->registerEngines(array(
        '.page' => 'supervillainhq\andkrupdk\www\cms\PageEngine',
//        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
}, true);

$di->set('applicationEventsManager', function () use ($di) {
	$evManager = new Manager();
	$exceptionHandler = new CmsExceptionHandler();
	$routeHandler = new CmsRouteHandler();

	$evManager->attach("micro:beforeHandleRoute", $routeHandler);
	$evManager->attach("micro:beforeException", $exceptionHandler);

	$evManager->attach("micro:afterHandleRoute", $routeHandler);

	$evManager->attach("micro", function ($event, $application) {
		// check if some domain-module must hook into the events
		$log = false;
	});
	return $evManager;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config, $auth) {
	$mysqlCfg = $config->db->mysql;
	$mysqlAuth = $auth->mysql;
    return new Mysql([
    	'host' => $mysqlCfg->host,
        'username' => $mysqlAuth->user,
        'password' => $mysqlAuth->password,
        'dbname' => $mysqlCfg->dbname,
        "options" => [
        	PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
		]
	]);
});


$di->setShared('mongo', function() use ($config) {
	$connection = "mongodb://{$config->mongo->host}:{$config->mongo->port}";
	$mongo = new \MongoDB\Client($connection);
	return $mongo->selectDatabase($config->mongo->dbname);
});

// Setting up the collection Manager
$di->set('collectionManager', function(){
	return new Phalcon\Mvc\Collection\Manager();
}, true);
