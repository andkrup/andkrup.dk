<?php
/**
 * config.php.
 *
 * TODO: Documentation required!
 */
return new Phalcon\Config([
	'application' => [
		'paths' => [
			'base' => '/var/www/andkrup.dk/apps/www',
			'views' => '/var/www/andkrup.dk/apps/www/resources/views',
			'docroot' => '/var/www/andkrup.dk/apps/www/public'
		]
	],
	'db' => [
		'mysql' => [
			'host' => 'localhost',
			'dbname' => 'andkrupDk'
		]
	],
	'mongo' => [
		'host' => 'localhost',
		'port' => '27017',
		'dbname' => 'andkrupDk',
	]
]);