<?php

namespace supervillainhq\andkrupdk\mongo {

	use MongoDB\Collection;
	use MongoDB\Database;
	use supervillainhq\spectre\DependencyInjecting;

	/**
	 * Class Document
	 * @package supervillainhq\andkrupdk\mongo
	 */
	abstract class Document{
		use DependencyInjecting;

		public $_id;

		protected static function parseBson(&$instance, \ArrayObject $data){
			$hashTable = $data->getArrayCopy();
			$keys = array_keys($hashTable);
			foreach ($keys as $key){
				$value = $hashTable[$key];
				if($value instanceof \ArrayObject){
					$instance->{$key} = new \stdClass();
					self::parseBson($instance->{$key}, $value);
				}
				else{
					$instance->{$key} = $value;
				}
			}
		}


		protected function getCollection(){
			$mongo = $this->getDI()->get('mongo');
			if($mongo instanceof Database) {
				$collection = $mongo->selectCollection($this->getSource());
				if ($collection instanceof Collection) {
					return $collection;
				}
			}
			return null;
		}
		protected function findOne(array $parameters){
			$collection = $this->getCollection();
			return $collection->findOne($parameters);
		}

		abstract public function getSource();

		abstract public function save();

		abstract public function update();

		abstract public function create();
	}
}


