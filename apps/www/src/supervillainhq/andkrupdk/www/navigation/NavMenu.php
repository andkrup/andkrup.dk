<?php

namespace supervillainhq\andkrupdk\www\navigation {

	use supervillainhq\andkrupdk\www\Htmlable;
	use supervillainhq\andkrupdk\www\HtmlElement;

	/**
	 * Class NavMenu
	 * @package supervillainhq\andkrupdk\www\navigation
	 */
	class NavMenu extends HtmlList implements HtmlElement{
		use Htmlable;

		static function load(){}

		function __construct(){
			$this->resetAttributes();
			$this->resetCssClasses();
			$this->name = "nav";
		}

		function innerHtml(){}

		function html(){
			$inner = $this->innerHtml();
			$start = $this->startTag();
			$end = $this->endTag();
			return "{$start}{$inner}{$end}";
		}
	}
}


