<?php

namespace supervillainhq\andkrupdk\www\navigation {

	use supervillainhq\andkrupdk\mongo\Document;

	/**
	 * Class HtmlList
	 * @package supervillainhq\andkrupdk\www\navigation
	 */
	class HtmlList extends Document{
		public $name;
		public $revision;
		public $items;

		public function getSource(){
			return "lists";
		}

		public function save(){
			// TODO: Implement save() method.
		}

		public function update(){
			// TODO: Implement update() method.
		}

		public function create(){
			// TODO: Implement create() method.
		}
	}
}


