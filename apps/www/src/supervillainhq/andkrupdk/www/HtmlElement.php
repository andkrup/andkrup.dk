<?php

namespace supervillainhq\andkrupdk\www {

	/**
	 * Class HtmlElement
	 * @package supervillainhq\andkrupdk\www
	 */
	interface HtmlElement{
		function getId();
		function setId($id);

		function getName();
		function setName($name);

		function addCssClass($cssClass);
		function hasCssClass($cssClass);
		function removeCssClass($cssClass);
		function cssClasses();
		function resetCssClasses($cssClasses = []);

		function addAttribute($attribute);
		function hasAttribute($attribute);
		function removeAttribute($attribute);
		function attributes();
		function resetAttributes($attributes = []);

		function innerHtml();
		function html();
	}
}


