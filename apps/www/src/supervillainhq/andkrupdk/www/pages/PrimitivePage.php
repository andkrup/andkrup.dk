<?php

namespace supervillainhq\andkrupdk\www\pages {

	use supervillainhq\andkrupdk\www\cms\Page;

	/**
	 * Class PrimitivePage
	 * @package supervillainhq\andkrupdk\cms\pages
	 */
	class PrimitivePage{
		static function convert(Page $page){
			$primitive = (object) [
				'id' => intval($page->id),
				'guid' => trim($page->guid),
				'revision' => intval($page->revision),
			];
			return $primitive;
		}
	}
}


