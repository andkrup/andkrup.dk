<?php

namespace supervillainhq\andkrupdk\www\pages {

	use supervillainhq\andkrupdk\www\cms\Page;

	/**
	 * Class PagesManager
	 * @package supervillainhq\andkrupdk\cms\pages
	 */
	class PagesManager{
		public function listPages(){
			$pages = Page::find();
			return $pages;
		}

		public function getPage($intval){
			$page = Page::findFirstById($intval);
			return $page;
		}
	}
}


