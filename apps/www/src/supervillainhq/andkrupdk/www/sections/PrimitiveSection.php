<?php

namespace supervillainhq\andkrupdk\www\sections {

	use supervillainhq\andkrupdk\www\cms\Section;

	/**
	 * Class PrimitiveSection
	 * @package supervillainhq\andkrupdk\www\sections
	 */
	class PrimitiveSection{
		static function convert(Section $section){
			$primitive = (object) [
				'name' => trim($section->name),
			];
			foreach (get_object_vars($section) as $property => $value) {
				$primitive->{$property} = $value;
			}
			return $primitive;
		}
	}
}


