<?php

namespace supervillainhq\andkrupdk\www {
	trait Htmlable{
		private $id;
		private $name;
		private $cssClasses;
		private $attributes;

		function getId(){
			return $this->id;
		}
		function setId($id){
			$this->id = $id;
		}

		function getName(){
			return $this->name;
		}
		function setName($name){
			$this->name = $name;
		}

		protected function startTag(){
			$id = empty($this->id) ? "" : htmlentities(trim($this->id));
			$cssClasses = empty($this->cssClasses) ? "" : htmlentities(implode(' ', $this->cssClasses));
			$name = htmlentities(trim($this->name));
			return "<{$name}{$id}{$cssClasses}>";
		}
		protected function endTag(){
			$name = htmlentities(trim($this->name));
			return "</{$name}>";
		}

		function addCssClass($cssClass){}
		function hasCssClass($cssClass){}
		function removeCssClass($cssClass){}
		function cssClasses(){
			return $this->cssClasses;
		}
		function resetCssClasses($cssClasses = []){
			$this->cssClasses = $cssClasses;
		}

		function addAttribute($attribute){}
		function hasAttribute($attribute){}
		function removeAttribute($attribute){}
		function attributes(){
			return $this->attributes;
		}
		function resetAttributes($attributes = []){
			$this->attributes = $attributes;
		}
	}
}


