<?php

namespace supervillainhq\andkrupdk\www\cms\user {

	use Phalcon\Mvc\Model;

	/**
	 * Class User
	 * @package supervillainhq\andkrupdk\www\cms\user
	 */
	class User extends Model{
		public $id;
		public $firstName;
		public $lastName;
		public $gender;
		public $login;
		public $password;
		public $created;
		public $modified;


		function initialize(){
			$this->setSource("Users");
		}
	}
}


