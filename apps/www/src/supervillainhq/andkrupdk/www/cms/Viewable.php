<?php

namespace supervillainhq\andkrupdk\www\cms {
	trait Viewable{
		protected $content;
		protected $viewsDir;
		protected $engines;


		/**
		 * Sets views directory. Depending of your platform, always add a trailing slash or backslash
		 *
		 * @param string $viewsDir
		 */
		public function setViewsDir($viewsDir){
			$this->viewsDir = $viewsDir;
		}

		/**
		 * Gets views directory
		 *
		 * @return string
		 */
		public function getViewsDir(){
			return $this->viewsDir;
		}

		/**
		 * Adds parameters to views (alias of setVar)
		 *
		 * @param string $key
		 * @param mixed $value
		 */
		public function setParamToView($key, $value){
			// TODO: Implement setParamToView() method.
		}

		/**
		 * Adds parameters to views
		 *
		 * @param string $key
		 * @param mixed $value
		 */
		public function setVar($key, $value){
			// TODO: Implement setVar() method.
		}

		/**
		 * Register templating engines
		 *
		 * @param array $engines
		 */
		public function registerEngines(array $engines){
			$this->engines = $engines;
		}

		/**
		 * Returns parameters to views
		 *
		 * @return array
		 */
		public function getParamsToView(){
			// TODO: Implement getParamsToView() method.
		}

		/**
		 * Returns the cache instance used to cache
		 *
		 * @return \Phalcon\Cache\BackendInterface
		 */
		public function getCache(){
			// TODO: Implement getCache() method.
		}

		/**
		 * Cache the actual view render to certain level
		 *
		 * @param mixed $options
		 */
		public function cache($options = true){
			// TODO: Implement cache() method.
		}

		/**
		 * Externally sets the view content
		 *
		 * @param string $content
		 */
		public function setContent($content){
			$this->content = $content;
		}

		/**
		 * Returns cached output from another view stage
		 *
		 * @return string
		 */
		public function getContent(){
			return $this->content;
		}

		/**
		 * Renders a partial view
		 *
		 * @param string $partialPath
		 * @param mixed $params
		 * @return string
		 */
		public function partial($partialPath, $params = null){
			// TODO: Implement partial() method.
		}

		/**
		 * Sets the layouts sub-directory. Must be a directory under the views directory. Depending of your platform, always add a trailing slash or backslash
		 *
		 * @param string $layoutsDir
		 */
		public function setLayoutsDir($layoutsDir){
			// TODO: Implement setLayoutsDir() method.
		}

		/**
		 * Gets the current layouts sub-directory
		 *
		 * @return string
		 */
		public function getLayoutsDir(){
			// TODO: Implement getLayoutsDir() method.
		}

		/**
		 * Sets a partials sub-directory. Must be a directory under the views directory. Depending of your platform, always add a trailing slash or backslash
		 *
		 * @param string $partialsDir
		 */
		public function setPartialsDir($partialsDir){
			// TODO: Implement setPartialsDir() method.
		}

		/**
		 * Gets the current partials sub-directory
		 *
		 * @return string
		 */
		public function getPartialsDir(){
			// TODO: Implement getPartialsDir() method.
		}

		/**
		 * Sets base path. Depending of your platform, always add a trailing slash or backslash
		 *
		 * @param string $basePath
		 */
		public function setBasePath($basePath){
			// TODO: Implement setBasePath() method.
		}

		/**
		 * Gets base path
		 *
		 * @return string
		 */
		public function getBasePath(){
			// TODO: Implement getBasePath() method.
		}

		/**
		 * Sets the render level for the view
		 *
		 * @param string $level
		 */
		public function setRenderLevel($level){
			// TODO: Implement setRenderLevel() method.
		}

		/**
		 * Sets default view name. Must be a file without extension in the views directory
		 *
		 * @param string $viewPath
		 */
		public function setMainView($viewPath){
			// TODO: Implement setMainView() method.
		}

		/**
		 * Returns the name of the main view
		 *
		 * @return string
		 */
		public function getMainView(){
			// TODO: Implement getMainView() method.
		}

		/**
		 * Change the layout to be used instead of using the name of the latest controller name
		 *
		 * @param string $layout
		 */
		public function setLayout($layout){
			// TODO: Implement setLayout() method.
		}

		/**
		 * Returns the name of the main view
		 *
		 * @return string
		 */
		public function getLayout(){
			// TODO: Implement getLayout() method.
		}

		/**
		 * Appends template before controller layout
		 *
		 * @param string|array $templateBefore
		 */
		public function setTemplateBefore($templateBefore){
			// TODO: Implement setTemplateBefore() method.
		}

		/**
		 * Resets any template before layouts
		 */
		public function cleanTemplateBefore(){
			// TODO: Implement cleanTemplateBefore() method.
		}

		/**
		 * Appends template after controller layout
		 *
		 * @param string|array $templateAfter
		 */
		public function setTemplateAfter($templateAfter){
			// TODO: Implement setTemplateAfter() method.
		}

		/**
		 * Resets any template before layouts
		 */
		public function cleanTemplateAfter(){
			// TODO: Implement cleanTemplateAfter() method.
		}

		/**
		 * Gets the name of the controller rendered
		 *
		 * @return string
		 */
		public function getControllerName(){
			// TODO: Implement getControllerName() method.
		}

		/**
		 * Gets the name of the action rendered
		 *
		 * @return string
		 */
		public function getActionName(){
			// TODO: Implement getActionName() method.
		}

		/**
		 * Gets extra parameters of the action rendered
		 *
		 * @return array
		 */
		public function getParams(){
			// TODO: Implement getParams() method.
		}

		/**
		 * Choose a view different to render than last-controller/last-action
		 *
		 * @param string $renderView
		 */
		public function pick($renderView){
			// TODO: Implement pick() method.
		}

		/**
		 * Finishes the render process by stopping the output buffering
		 */
		public function finish(){
			// TODO: Implement finish() method.
		}

		/**
		 * Returns the path of the view that is currently rendered
		 *
		 * @return string
		 */
		public function getActiveRenderPath(){
			// TODO: Implement getActiveRenderPath() method.
		}

		/**
		 * Disables the auto-rendering process
		 */
		public function disable(){
			// TODO: Implement disable() method.
		}

		/**
		 * Enables the auto-rendering process
		 */
		public function enable(){
			// TODO: Implement enable() method.
		}

		/**
		 * Whether the automatic rendering is disabled
		 *
		 * @return bool
		 */
		public function isDisabled(){
			// TODO: Implement isDisabled() method.
		}
	}
}


