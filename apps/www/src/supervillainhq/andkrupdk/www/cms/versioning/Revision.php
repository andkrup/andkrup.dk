<?php

namespace supervillainhq\andkrupdk\www\cms\versioning {

	/**
	 * Class Revision
	 * @package supervillainhq\andkrupdk\www\cms\versioning
	 */
	class Revision{
		private $id;
		private $created;

		public function getId(){
			return $this->id;
		}
		public function setId($id){
			$this->id = $id;
		}

		public function setCreated(\DateTime $created){
			$this->created = $created;
		}
		public function getCreated(){
			return $this->created;
		}
	}
}


