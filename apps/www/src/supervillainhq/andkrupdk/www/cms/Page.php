<?php

namespace supervillainhq\andkrupdk\www\cms {

	use Phalcon\Mvc\Model;
	use Phalcon\Mvc\ViewInterface;

	/**
	 * Class Page
	 * @package supervillainhq\andkrupdk\www\cms
	 */
	class Page extends Model implements ViewInterface{
		use Viewable;

		public $id;
		public $revision;
		public $guid;
		public $created;
		public $modified;

		private $sections;
		private $layout;
		private $engine;


		private static function fetchSections(Page $page){
			if($collection = SectionCollection::findFirstByPage($page)){
//				$collection->page_id = $page->id;
				return $collection;
			}
			return null;
		}
		private static function fetchPageLayout(Page $page){
			if($layout = PageLayout::findFirstByPage($page)){
//				$layout->page_id = $page->id;
				return $layout;
			}
			return null;
		}


		function getSections(){
			if(is_null($this->sections)){
				$this->sections = self::fetchSections($this);
			}
			return $this->sections;
		}
		function setSections(SectionCollection $sections){
			$sections->page_id = $this->id;
			$this->sections = $sections;
		}

		public function getPageLayout(){
			if(is_null($this->layout)){
				$this->layout = self::fetchPageLayout($this);
			}
			return $this->layout;
		}
		public function setPageLayout(PageLayout $layout){
			$layout->page_id = $this->id;
			$this->layout = $layout;
		}

		function initialize(){
			$this->setSource("Pages");
		}

		/**
		 * Starts rendering process enabling the output buffering
		 */
		public function start(){
			$this->engine = $this->engines['.page']($this, $this->getDI());
		}


		/**
		 * Executes render process from dispatching data
		 *
		 * @param string $controllerName
		 * @param string $actionName
		 * @param array $params
		 */
		public function render($controllerName, $actionName, $params = null){
			$parameters = [];
			$this->engine->render($this->viewsDir, $parameters);
		}

	}
}


