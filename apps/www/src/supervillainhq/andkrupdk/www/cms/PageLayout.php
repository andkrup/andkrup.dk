<?php

namespace supervillainhq\andkrupdk\www\cms {

	use Phalcon\Di;
	use Phalcon\DiInterface;
	use supervillainhq\andkrupdk\mongo\Document;
	use supervillainhq\spectre\DependencyInjecting;

	/**
	 * Class PageLayout
	 * @package supervillainhq\andkrupdk\www\cms
	 */
	class PageLayout extends Document{
		use DependencyInjecting;

		public $page_id;

		private $path;
		private $sections;

		function __construct(DiInterface $dependencyInjector){
			$this->setDI($dependencyInjector);
		}


		public static function findFirstByPage(Page $page){
			$instance = new PageLayout(Di::getDefault());
			if($document = $instance->findOne(['page_id' => intval($page->id)])){
				Document::parseBson($instance, $document);
			}
			return $instance;
		}

		public function getSource(){
			return "layouts";
		}

		function setPath($path){
			$this->path = $path;
		}
		public function getPath(){
			return $this->path;
		}

		/**
		 *
		 * @param SectionCollection $sections
		 */
		function sections(SectionCollection $sections){
			$this->sections = $sections;
		}

		function html(){
			$template = $this->path;

			$buffer = '';

			if($this->sections instanceof SectionCollection){
				$sections = $this->sections->sections();
				foreach ($sections as $section) {
					$buffer .= $section->html();
				}
			}
			else{
				$buffer = 'raw body contents';
			}

			$parameters = [
				'metaTags' => [],
				'title' => 'Sectioned Title',
				'styles' => [
					'css/reset.css',
					'css/fonts.css',
					'css/default.css',
				],
				'scripts' => [],
				'body' => $buffer,
				'footerScripts' => [
					'js/default.bundle.js'
				],
			];
			$scope = function($parameters) use($template){
				extract($parameters);
				unset($parameters);
				ob_start();
				include $template;
				return ob_get_clean();
			};
			return $scope($parameters);
		}

		public function save(){
			// TODO: Implement save() method.
		}

		public function update(){
			// TODO: Implement update() method.
		}

		public function create(){
			// TODO: Implement create() method.
		}
	}
}


