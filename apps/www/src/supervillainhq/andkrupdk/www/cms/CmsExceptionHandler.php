<?php

namespace supervillainhq\andkrupdk\www\cms {

	use Phalcon\Events\Event;
	use Phalcon\Mvc\Micro;
	use Phalcon\Http\Response;

	/**
	 * Class CmsExceptionHandler
	 * @package supervillainhq\andkrupdk\www\cms
	 */
	class CmsExceptionHandler{
		function beforeException(Event $event, Micro $application, \Throwable $exception) {
			$response = new Response();
			$response->setContent($exception->getMessage());
			if($exception instanceof \HttpException){
				$code = $exception->getCode();
				if($code === 0){
					$code = 500;
				}
			}
			else{
				$code = 500;
			}
			$response->setStatusCode($code);
			$response->send();
			exit;
//			return false;
		}
	}
}


