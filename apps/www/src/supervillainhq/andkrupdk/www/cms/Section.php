<?php

namespace supervillainhq\andkrupdk\www\cms {

	use supervillainhq\andkrupdk\www\cms\backoffice\editors\Editor;

	/**
	 * Class Section
	 * @package supervillainhq\andkrupdk\www\cms
	 */
	class Section{
		public $name;
		public $title;
		public $html;
		public $revision;
		private $editor;


		static function parse($data){
			$props = (object) $data;
			$instance = new Section();
			$instance->name = trim($props->name);
			foreach (get_object_vars($props) as $prop => $value) {
				if('name' != $prop){
					$instance->{$prop} = $value;
				}
			}
			return $instance;
		}

		function revision(){}

		function setEditor(Editor $editor){}
		function getEditor(){}

		/**
		 * Returns the html content of this instance.
		 *
		 * @return string
		 */
		function html(){
			return nl2br(trim($this->html));
		}
	}
}


