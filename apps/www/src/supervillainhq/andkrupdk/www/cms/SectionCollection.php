<?php

namespace supervillainhq\andkrupdk\www\cms {

	use Phalcon\Di;
	use Phalcon\DiInterface;
	use supervillainhq\andkrupdk\mongo\Document;
	use supervillainhq\spectre\DependencyInjecting;

	/**
	 * Class SectionCollection
	 * @package supervillainhq\andkrupdk\www\cms
	 */
	class SectionCollection extends Document{
		use DependencyInjecting;

		public $page_id;


		function __construct(DiInterface $dependencyInjector){
			$this->setDI($dependencyInjector);
		}


		public static function findFirstByPage(Page $page){
			$instance = new SectionCollection(Di::getDefault());
			if($document = $instance->findOne(['page_id' => intval($page->id)])){
				Document::parseBson($instance, $document);
			}
			return $instance;
		}

		function sections(){
			if(property_exists($this, 'sections')){
				$sections = [];
				foreach ($this->sections as $name => $section) {
					$section->name = $name;
					array_push($sections, Section::parse($section));
				};
				return $sections;
			}
			return null;
		}


		public function getSource(){
			return "sections";
		}

		public function save(){
			// TODO: Implement save() method.
		}

		public function update(){
			// TODO: Implement update() method.
		}

		public function create(){
			// TODO: Implement create() method.
		}
	}
}


