<?php

namespace supervillainhq\andkrupdk\www\cms {

	use Phalcon\Config;
	use Phalcon\Mvc\View\Engine;
	use Phalcon\Mvc\View\EngineInterface;

	/**
	 * Class PageEngine
	 * @package supervillainhq\andkrupdk\www\cms
	 */
	class PageEngine extends Engine implements EngineInterface{

		private $page;
		private $viewsPath;

		function __construct($view, $dependencyInjector){
			parent::__construct($view, $dependencyInjector);
			$this->page = $view;
		}


		private function setViewsBasePath($viewsPath){
			$this->viewsPath = $viewsPath;
		}
		function getViewsBasePath(){
			return $this->viewsPath;
		}


		static function create($guid, Config $config){
			$page = Page::findFirstByGuid($guid);
			if(!$page){
				throw new \Exception('not found', 404);
			}

			$page->registerEngines([
					".page" => function($page, $di) use($config) {
						$engine = new PageEngine($page, $di);
						$engine->setViewsBasePath($config->application->paths->views);
						return $engine;
					}
				]
			);
			return $page;
		}

		/**
		 * Renders a view using the template engine
		 *
		 * @param string $path
		 * @param mixed $params
		 * @param bool $mustClean
		 */
		public function render($path, $params, $mustClean = false){
			$sections = $this->page->getSections();
			$layout = $this->page->getPageLayout();
			// TODO: the $layout should be able to dictate the below file reference 'index.html'
			$view = is_null($path) ? 'index.phtml' : $path;
			$path = realpath("{$this->viewsPath}/{$view}");
			$layout->setPath($path);
			$layout->sections($sections);
			$buffer = $layout->html();
			$this->page->setContent($buffer);
		}
	}
}


