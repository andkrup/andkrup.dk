<?php

namespace supervillainhq\andkrupdk\www\cms\routing {

	use Phalcon\DiInterface;
	use Phalcon\Http\Response;
	use supervillainhq\spectre\DependencyInjecting;

	/**
	 * Class CmsController
	 * @package supervillainhq\andkrupdk\www\cms\routing
	 */
	class CmsController{
		use DependencyInjecting;


		function __construct(DiInterface $dependencyInjector){
			$this->setDI($dependencyInjector);
		}

		function index(){
			return $this->page('index');
		}

		function page($pageGuid){
			$di = $this->getDI();
			$page = $di->getPage($pageGuid);
			$template = null;
			$page->start();
			$page->render($template, null);
			$filestream = $page->getContent();
			$response = new Response();
			$response->resetHeaders();
			$response->setContentType('text/html', 'UTF-8');
			$response->setHeader('X-Powered-By', 'Supervillain HQ');
			$response->setContent($filestream);
			return $response;
		}

		function folderPage($folder, $pagename){
			$response = new Response();
			$response->setContent("{$folder} > {$pagename}");
			return $response;
		}
	}
}


