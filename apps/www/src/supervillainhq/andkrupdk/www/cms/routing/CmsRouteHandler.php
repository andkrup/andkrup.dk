<?php

namespace supervillainhq\andkrupdk\www\cms\routing {

	use Phalcon\Events\Event;
	use Phalcon\Mvc\Micro;

	/**
	 * Class CmsRouteHandler
	 * @package supervillainhq\andkrupdk\www\cms\routing
	 */
	class CmsRouteHandler{
		function afterHandleRoute(Event $event, Micro $application) {
			// ensure that a Response implementation is returned to the browser
			if($response = $application->getReturnedValue()){
				if(is_scalar($response)){
					throw new \Exception('bad response');
				}

				$interfaces = class_implements($response);
				if(is_array($interfaces) && in_array('Phalcon\Http\ResponseInterface', $interfaces)){
					$response->send();
					exit;
				}
			}
			throw new \Exception('bad response');
		}

		function beforeHandleRoute(Event $event, Micro $application) {
			$log = false;
		}
	}
}


