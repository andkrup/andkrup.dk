<?php

namespace supervillainhq\andkrupdk\admin {

	use Phalcon\DiInterface;
	use Phalcon\Loader;
	use Phalcon\Mvc\Dispatcher;
	use Phalcon\Mvc\ModuleDefinitionInterface;
	use Phalcon\Mvc\View;


	/**
	 * The Main module managages general web site settings.
	 *
	 * @package supervillainhq\andkrupdk\admin
	 */
	class Main implements ModuleDefinitionInterface{

		static function moduleDefinition(){
			return [
				'className' => 'supervillainhq\andkrupdk\admin\Main',
				'path' => __FILE__,
			];
		}

		/**
		 * Registers an autoloader related to the module
		 *
		 * @param mixed $dependencyInjector
		 */
		public function registerAutoloaders(DiInterface $dependencyInjector = null){
			$loader = new Loader();
			$config = $dependencyInjector->get('config');

			$loader->registerNamespaces([
				'supervillainhq\andkrupdk\admin\main\controllers' => "{$config->application->paths->base}/src/supervillainhq/andkrupdk/admin/main/controllers",
				'supervillainhq\andkrupdk\admin\main' => "{$config->application->paths->base}/src/supervillainhq/andkrupdk/admin/main",
				'supervillainhq\andkrupdk\www' => "{$config->application->paths->frontApp}/src/supervillainhq/andkrupdk/www",
				'supervillainhq\andkrupdk\mongo' => "{$config->application->paths->frontApp}/src/supervillainhq/andkrupdk/mongo"
			]);

			$loader->register();
		}

		/**
		 * Registers services related to the module
		 *
		 * @param mixed $dependencyInjector
		 */
		public function registerServices(DiInterface $dependencyInjector){
			$config = $dependencyInjector->get('config');
			$dependencyInjector->set("view", function () use ($config){
				$view = new View();
				$view->setViewsDir("{$config->application->paths->resources}/main/views/");
				return $view;
			});
			$dependencyInjector->set("dispatcher", function () {
				$dispatcher = new Dispatcher();
				$dispatcher->setDefaultNamespace('supervillainhq\andkrupdk\admin\main\controllers');
				return $dispatcher;
			});
		}
	}
}


