<?php

namespace supervillainhq\andkrupdk\admin\ajax\controllers {

	use Phalcon\Mvc\Controller;

	/**
	 * Class NewsController
	 * @package supervillainhq\andkrupdk\admin\ajax\controllers
	 */
	class NewsController extends Controller{
		function listAction(){
			$val = (object) [
				'response' => [
					(object) [
						'title' => 'Heads up!',
						'text' => 'Important news concerning your favourite CMS',
						'published' => '2016-11-20 01:55:00',
					],
					(object) [
						'title' => 'Yo!!',
						'text' => 'Important news concerning your favourite CMS',
						'published' => '2016-11-20 01:55:00',
					],
					(object) [
						'title' => 'I have words!',
						'text' => 'Important news concerning your favourite CMS',
						'published' => '2016-11-20 01:55:00',
					],
				],
			];

			$response = $this->response;
			$response->resetHeaders();
			$response->setContent(json_encode($val));
			$response->setContentType('application/json');
			return $response;
		}
	}
}


