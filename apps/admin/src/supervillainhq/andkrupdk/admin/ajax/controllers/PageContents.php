<?php

namespace supervillainhq\andkrupdk\admin\ajax\controllers {

	use Phalcon\Mvc\Controller;
	use supervillainhq\andkrupdk\www\pages\PagesManager;

	/**
	 * Class PageContents
	 * @package supervillainhq\andkrupdk\admin\ajax\controllers
	 */
	class PageContents extends Controller{
		/**
		 * Return the Section instances that are related to a Page instance.
		 *
		 * @param $pageId
		 * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
		 */
		function sectionsAction($pageId){
			$pageService = new PagesManager();
			$page = $pageService->getPage(intval($pageId));
			$sections = $page->getSections();

			$response = $this->response;
			$response->resetHeaders();
			$response->setContent(json_encode($sections));
			$response->setContentType('application/json');
			return $response;
		}
	}
}


