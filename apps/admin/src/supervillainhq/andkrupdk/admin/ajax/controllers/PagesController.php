<?php

namespace supervillainhq\andkrupdk\admin\ajax\controllers {

	use Phalcon\Mvc\Controller;
	use supervillainhq\andkrupdk\www\pages\PagesManager;
	use supervillainhq\andkrupdk\www\pages\PrimitivePage;

	/**
	 * Class PagesController
	 * @package supervillainhq\andkrupdk\admin\ajax\controllers
	 */
	class PagesController extends Controller{

		function indexAction(){
			$pagesManager = new PagesManager();
			$pages = $pagesManager->listPages();
			$primitives = [];

			foreach ($pages as $page) {
				array_push($primitives, PrimitivePage::convert($page));
			}

			$response = $this->response;
			$response->resetHeaders();
			$response->setContent(json_encode($primitives));
			$response->setContentType('application/json');
			return $response;
		}

		function getAction($pageId){
			$val = (object) [
				'response' => (object) [
					'page' => "page #{$pageId} data not yet available"
				],
			];

			$response = $this->response;
			$response->resetHeaders();
			$response->setContent(json_encode($val));
			$response->setContentType('application/json');
			return $response;
		}

		function createAction(){
			if(!$this->request->isPost()){
				throw new \Exception('method not allowed');
			}
			$val = (object) [
				'response' => "create page",
			];

			$response = $this->response;
			$response->resetHeaders();
			$response->setContent(json_encode($val));
			$response->setContentType('application/json');
			return $response;
		}

		function updateAction($pageId){
			$val = (object) [
				'response' => "update page {$pageId}",
			];

			$response = $this->response;
			$response->resetHeaders();
			$response->setContent(json_encode($val));
			$response->setContentType('application/json');
			return $response;
		}

		function archiveAction($pageId){
			$val = (object) [
				'response' => "delete page {$pageId}",
			];

			$response = $this->response;
			$response->resetHeaders();
			$response->setContent(json_encode($val));
			$response->setContentType('application/json');
			return $response;
		}
	}
}


