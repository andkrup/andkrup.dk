<?php

namespace supervillainhq\andkrupdk\admin\ajax\controllers {

	use Phalcon\Mvc\Controller;
	use supervillainhq\andkrupdk\www\pages\PagesManager;
	use supervillainhq\andkrupdk\www\sections\PrimitiveSection;

	/**
	 * Class SectionController
	 * @package supervillainhq\andkrupdk\admin\ajax\controllers
	 */
	class SectionController extends Controller{
		function indexAction(){
			$request = $this->request;
			if($pageId = $request->get('page')){
				$pageService = new PagesManager();
				$page = $pageService->getPage(intval($pageId));
				$sections = $page->getSections();
				$primitives = [];
				foreach ($sections as $section) {
					array_push($primitives, PrimitiveSection::convert($section));
				}
				$response = $this->response;
				$response->resetHeaders();
				$response->setContent(json_encode($primitives));
				$response->setContentType('application/json');
				return $response;
			}
		}

		function getAction($pageId){
			$val = (object) [
				'response' => (object) [
					'page' => "page #{$pageId} data not yet available"
				],
			];

			$response = $this->response;
			$response->resetHeaders();
			$response->setContent(json_encode($val));
			$response->setContentType('application/json');
			return $response;
		}

		function createAction(){
			if(!$this->request->isPost()){
				throw new \Exception('method not allowed');
			}
			$val = (object) [
				'response' => "create page",
			];

			$response = $this->response;
			$response->resetHeaders();
			$response->setContent(json_encode($val));
			$response->setContentType('application/json');
			return $response;
		}

		function updateAction($pageId){
			$val = (object) [
				'response' => "update page {$pageId}",
			];

			$response = $this->response;
			$response->resetHeaders();
			$response->setContent(json_encode($val));
			$response->setContentType('application/json');
			return $response;
		}

		function archiveAction($pageId){
			$val = (object) [
				'response' => "delete page {$pageId}",
			];

			$response = $this->response;
			$response->resetHeaders();
			$response->setContent(json_encode($val));
			$response->setContentType('application/json');
			return $response;
		}
	}
}


