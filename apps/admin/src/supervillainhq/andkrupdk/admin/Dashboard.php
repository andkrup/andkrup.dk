<?php

namespace supervillainhq\andkrupdk\admin {

	use Phalcon\DiInterface;
	use Phalcon\Loader;
	use Phalcon\Mvc\Dispatcher;
	use Phalcon\Mvc\ModuleDefinitionInterface;
	use Phalcon\Mvc\View;

	/**
	 * The Dashboard module manages your personal settings
	 *
	 * @package supervillainhq\andkrupdk\admin
	 */
	class Dashboard implements ModuleDefinitionInterface{

		static function moduleDefinition(){
			return [
				'className' => 'supervillainhq\andkrupdk\admin\Dashboard',
				'path' => __FILE__,
			];
		}

		/**
		 * Register a specific autoloader for the module
		 * @param DiInterface $dependencyInjector
		 */
		public function registerAutoloaders(DiInterface $dependencyInjector = null){
			$loader = new Loader();
			$config = $dependencyInjector->get('config');

			$loader->registerNamespaces([
				'supervillainhq\andkrupdk\admin\dashboard\controllers' => "{$config->application->paths->base}/src/supervillainhq/andkrupdk/admin/dashboard/controllers",
				'supervillainhq\andkrupdk\admin\dashboard' => "{$config->application->paths->base}/src/supervillainhq/andkrupdk/admin/dashboard",
			]);

			$loader->register();
		}

		/**
		 * Register specific services for the module
		 * @param DiInterface $dependencyInjector
		 */
		public function registerServices(DiInterface $dependencyInjector){
			$config = $dependencyInjector->get('config');
			$dependencyInjector->set("view", function () use ($config){
				$view = new View();
				$view->setViewsDir("{$config->application->paths->resources}/dashboard/views/");
				$view->setLayoutsDir("{$config->application->paths->resources}/shared/layouts/");
				return $view;
			});
			$dependencyInjector->set("dispatcher", function () {
				$dispatcher = new Dispatcher();
				$dispatcher->setDefaultNamespace('supervillainhq\andkrupdk\admin\dashboard\controllers');
				return $dispatcher;
			});
		}
	}
}


