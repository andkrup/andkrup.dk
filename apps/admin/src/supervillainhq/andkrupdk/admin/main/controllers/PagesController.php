<?php

namespace supervillainhq\andkrupdk\admin\main\controllers {

	use Phalcon\Mvc\Controller;
	use supervillainhq\andkrupdk\www\pages\PagesManager;

	/**
	 * Class PagesController
	 * @package supervillainhq\andkrupdk\admin\main\controllers
	 */
	class PagesController extends Controller{
		function indexAction(){}

		function editAction($pageId){
			$pageService = new PagesManager();
			$page = $pageService->getPage(intval($pageId));
			$sections = $page->getSections();
		}
	}
}


