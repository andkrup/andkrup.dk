<?php

namespace supervillainhq\andkrupdk\admin {

	use Phalcon\DiInterface;
	use Phalcon\Loader;
	use Phalcon\Mvc\Dispatcher;
	use Phalcon\Mvc\ModuleDefinitionInterface;
	use Phalcon\Mvc\View;


	/**
	 * The Ajax module facilitates service access to the backend.
	 *
	 * @package supervillainhq\andkrupdk\admin
	 */
	class Ajax implements ModuleDefinitionInterface{

		static function moduleDefinition(){
			return [
				'className' => 'supervillainhq\andkrupdk\admin\Ajax',
				'path' => __FILE__,
			];
		}

		/**
		 * Registers an autoloader related to the module
		 *
		 * @param mixed $dependencyInjector
		 */
		public function registerAutoloaders(DiInterface $dependencyInjector = null){
			$loader = new Loader();
			$config = $dependencyInjector->get('config');

			$loader->registerNamespaces([
				'supervillainhq\andkrupdk\admin\ajax\controllers' => "{$config->application->paths->base}/src/supervillainhq/andkrupdk/admin/ajax/controllers",
				'supervillainhq\andkrupdk\admin\ajax' => "{$config->application->paths->base}/src/supervillainhq/andkrupdk/admin/ajax",
				'supervillainhq\andkrupdk\www' => "{$config->application->paths->frontApp}/src/supervillainhq/andkrupdk/www",
				'supervillainhq\andkrupdk\mongo' => "{$config->application->paths->frontApp}/src/supervillainhq/andkrupdk/mongo"
			]);

			$loader->register();
		}

		/**
		 * Registers services related to the module
		 *
		 * @param mixed $dependencyInjector
		 */
		public function registerServices(DiInterface $dependencyInjector){
			$config = $dependencyInjector->get('config');
			$dependencyInjector->set("view", function () use ($config){
				$view = new View();
				$view->setViewsDir("{$config->application->paths->resources}/ajax/views/");
				$view->setLayoutsDir("{$config->application->paths->resources}/shared/layouts/");
				$view->setRenderLevel(View::LEVEL_NO_RENDER);
				return $view;
			});
			$dependencyInjector->set("dispatcher", function () {
				$dispatcher = new Dispatcher();
				$dispatcher->setDefaultNamespace('supervillainhq\andkrupdk\admin\ajax\controllers');
				return $dispatcher;
			});
		}
	}
}


