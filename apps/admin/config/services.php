<?php
/**
 * services.php.
 */
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Collection\Manager;
use Phalcon\Db\Adapter\Pdo\Mysql;
use supervillainhq\andkrupdk\www\cms\PageEngine;

$di = new FactoryDefault();

$di->set('config', $config);

$di->set('page', function($guid) use ($config){
	return PageEngine::create($guid, $config);
});

$di->set('router', function () use ($di) {
	$router = new Router();
	$router->setDefaultModule('dashboard');
	$routes = include 'routes.php';
	foreach ($routes as $expression => $route) {
		$route = (object) $route;
		$paths = $route->paths;
		if(in_array('get', $route->methods)){
			$router->addGet($expression, $paths);
		}
		if(in_array('post', $route->methods)){
			$router->addPost($expression, $paths);
		}
	}
	return $router;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config, $auth) {
	$mysqlCfg = $config->db->mysql;
	$mysqlAuth = $auth->mysql;
    return new Mysql([
    	'host' => $mysqlCfg->host,
        'username' => $mysqlAuth->user,
        'password' => $mysqlAuth->password,
        'dbname' => $mysqlCfg->dbname,
        "options" => [
        	PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
		]
	]);
});


$di->setShared('mongo', function() use ($config) {
	$connection = "mongodb://{$config->mongo->host}:{$config->mongo->port}";
	$mongo = new \MongoDB\Client($connection);
	return $mongo->selectDatabase($config->mongo->dbname);
});

// Setting up the collection Manager
$di->set('collectionManager', function(){
	return new Phalcon\Mvc\Collection\Manager();
}, true);
