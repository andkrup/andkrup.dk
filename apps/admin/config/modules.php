<?php
/**
 * modules.php.
 *
 * TODO: Documentation required!
 */
return [
	'dashboard' => \supervillainhq\andkrupdk\admin\Dashboard::moduleDefinition(),
	'ajax' => \supervillainhq\andkrupdk\admin\Ajax::moduleDefinition(),
	'main' => \supervillainhq\andkrupdk\admin\Main::moduleDefinition()
];