<?php
/**
 * loader.php.
 *
 * Only loads Composer dependencies
 */

require_once __DIR__ . '/../../../vendor/autoload.php';
