<?php
/**
 * routes.php.
 *
 * Defines all routes into the available modules
 */
return [
	"/dashboard/:controller/:action" => [
		'paths' => [
			"module"     => "dashboard",
			"controller" => 1,
			"action"     => 2,
		],
		'methods' => ['get'],
	],
	"/dashboard/:controller" => [
		'paths' => [
			"module"     => "dashboard",
			"controller" => 1,
			"action"     => 'index',
		],
		'methods' => ['get'],
	],
	"/dashboard" => [
		'paths' => [
			"module" => "dashboard",
			"controller" => 'index',
			"action" => 'index',
		],
		'methods' => ['get'],
	],
	"/main/:controller/:action/:params" => [
		'paths' => [
			"module" => "main",
			"controller" => 1,
			"action" => 2,
			"params" => 3,
		],
		'methods' => ['get'],
	],
	"/main/:controller/:action" => [
		'paths' => [
			"module" => "main",
			"controller" => 1,
			"action" => 2,
		],
		'methods' => ['get'],
	],
	"/main/:controller" => [
		'paths' => [
			"module" => "main",
			"controller" => 1,
			"action" => 'index',
		],
		'methods' => ['get'],
	],
	"/main" => [
		'paths' => [
			"module"     => "main",
			"controller" => 'index',
			"action"     => 'index',
		],
		'methods' => ['get'],
	],
	"/ajax/:controller/:action/:params" => [
		'paths' => [
			"module"     => "ajax",
			"controller" => 1,
			"action"     => 2,
			"params"     => 3,
		],
		'methods' => ['get','post'],
	],
	"/ajax/:controller/:action" => [
		'paths' => [
			"module"     => "ajax",
			"controller" => 1,
			"action"     => 2,
		],
		'methods' => ['get','post'],
	],
	"/ajax/:controller" => [
		'paths' => [
			"module"     => "ajax",
			"controller" => 1,
			"action"     => 'index',
		],
		'methods' => ['get','post'],
	]
];