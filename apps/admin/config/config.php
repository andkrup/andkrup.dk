<?php
/**
 * config.php.
 *
 * TODO: Documentation required!
 */
return new Phalcon\Config([
	'application' => [
		'paths' => [
			'frontApp' => '/var/www/andkrup.dk/apps/www',
			'base' => '/var/www/andkrup.dk/apps/admin',
			'resources' => '/var/www/andkrup.dk/apps/admin/resources',
			'docroot' => '/var/www/andkrup.dk/apps/admin/public'
		]
	],
	'db' => [
		'mysql' => [
			'host' => 'localhost',
			'dbname' => 'andkrupDk'
		]
	],
	'mongo' => [
		'host' => 'localhost',
		'port' => '27017',
		'dbname' => 'andkrupDk',
	]
]);