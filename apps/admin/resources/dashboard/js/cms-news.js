/**
 * Created by anders on 20/11/16.
 */

var AmpersandRestCollection = require('ampersand-rest-collection');

var CmsNews = AmpersandRestCollection.extend({
	url : '/ajax/news/list'
});

// cmsNews.prototype.load = function(){
// 	console.log('load news from: ' + this.uri);
// };

//export squareNumbers as the module function
module.exports = CmsNews;