/**
 * Created by anders on 20/11/16.
 */
console.log('Dashboard');

var CmsNews = require('./cms-news');
var templates = require('../../../../../build/templates.js');
var dom = require('ampersand-dom');
var app = require('ampersand-app');
var _ = require('lodash');
var domReady = require('domready');

// attach our app to `window` so we can
// easily access it from the console.
window.app = app;

// Extends our main app singleton
app.extend({
	newsService : new CmsNews(),
    // This is where it all starts
    init: function() {
		this.newsService.fetch({
			success: function(collection, response, options){
				var el = document.getElementsByClassName('news-items')[0];
				var html = [];

				for(var i in response.response){
					var entry = response.response[i];
					var parameters = {
						title: entry.title,
						text: entry.text
					};
					html.push(templates.news(parameters));
				}
				dom.html(el, html.join('\n'));
				dom.show(el);
				// console.log(collection);
				// console.log(response.response);
			}
		});
    },

	onCmsNews: function(collection, response, options){
		console.log(collection);
		console.log(response);
	}
});

domReady(_.bind(app.init, app));