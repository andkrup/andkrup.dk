/**
 * Created by anders on 20/11/16.
 */

var AmpersandModel = require('ampersand-model');

module.exports = AmpersandModel.extend({
	props: {
		id: 'number',
		guid: 'string',
		revision: 'number'
	},
	urlRoot : '/ajax/pages'
});
