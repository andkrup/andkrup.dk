/**
 * Created by anders on 29/12/16.
 */
var Page = require('./page');
var AmpersandRestCollection = require('ampersand-rest-collection');

module.exports = AmpersandRestCollection.extend({
	model: Page,
	url : '/ajax/pages'
});
