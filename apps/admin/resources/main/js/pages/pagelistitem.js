/**
 * Created by anders on 29/12/16.
 */
var AmpersandView = require('ampersand-view');
var templates = require('../../../../../../build/templates.main.js');

module.exports = AmpersandView.extend({
	bindings : {
		'model.guid': {
            type: 'innerHTML',
            selector: '.page-guid'
        },
		'model.revision': {
            type: 'text',
            selector: '.page-revision'
        }
	},
	events: {
		"click .page-option-edit": function(){
			window.location.href = "/main/pages/edit/" + this.model.getId();
		}
	},
	template: function(){
		return templates.pages.pagelistitem();
	}
});