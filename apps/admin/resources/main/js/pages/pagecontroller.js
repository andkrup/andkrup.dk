/**
 * Created by anders on 29/12/16.
 */
var AmpersandState = require('ampersand-state');
var PageService = require('./pages');
var SectionService = require('../sections/sections');
var PagelistItemView = require('./pagelistitem');
var PagelistView = require('./pagelist');
var SectionOverlayEditor = require('../sections/SectionOverlayEditor');
var SectionSelectOptionView = require('../sections/pagesectionselectboxoption');
var SectionSelectBoxView = require('../sections/pagesectionselectbox');

module.exports = AmpersandState.extend({
	pageSectionSelectBoxView: undefined,

	index: function(){
		var pages = new PageService();
		var pageslistView = new PagelistView({
	    	el: document.getElementById('pages-table')
		});
		pages.fetch({
			success: function(collection, response, options){
				pageslistView.renderCollection(collection, PagelistItemView, '#pages-container');
			}
		});
	},
	onEditPageSection: function(){
		var container = document.getElementsByClassName('overlay-container')[0];
		console.log(container);
		if(typeof container == 'undefined'){
			container = document.createElement('div');
			container.classList.add('overlay-container');
			document.body.appendChild(container);
		}

		var overlay = new SectionOverlayEditor({
			el: container
		});
		overlay.display();
	},
	onPageSections: function(collection, response, options){
		var scope = this;
		this.pageSectionSelectBoxView = new SectionSelectBoxView({
	    	el: document.getElementById('page-sections')
		});
		this.pageSectionSelectBoxView.renderCollection(collection, function(options){
			options.controller = scope;
			options.onDblClick = function(){
				// console.log(scope);
				scope.onEditPageSection();
			};
			return new SectionSelectOptionView(options);
		});
	},
	/**
	 *
	 * @param pageId
	 */
	edit: function(pageId){
		var scope = this;
		var sections = new SectionService();
		sections.fetch({
			data: {
				page: pageId
			},
			// controller: this,
			success: function(collection, response, options){
				scope.onPageSections(collection, response, options);
			}
		});
	}
});