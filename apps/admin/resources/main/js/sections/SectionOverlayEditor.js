/**
 * Created by anders on 29/12/16.
 */
var AmpersandView = require('ampersand-view');
var AmpersandDom = require('ampersand-dom');

module.exports = AmpersandView.extend({
	bindings : {

	},
	template: '<div class="overlay"></div>',

	display: function(){
		this.render();
	},
	hide: function(){}
});