/**
 * Created by anders on 29/12/16.
 */
var AmpersandView = require('ampersand-view');
var templates = require('../../../../../../build/templates.main.js');

module.exports = AmpersandView.extend({
	initialize: function(options){
		this.controller = options.controller;
		this.onDblClick = options.onDblClick;
	},
	bindings : {
		'model.name': {
            type: 'innerHTML'
        }
	},
	events: {
		"click": function(){
			console.log(this.model.get('name'));
		},
		"dblclick": function(){
			// console.log(this.controller);
			this.onDblClick.apply(this.controller, [this.model.get('name')]);
			// console.log('open in overlay for edit: ' + this.model.get('name'));
			// var overlay = new SectionOverlayEditor({
			// 	el: document.getElementsByTagName('body')[0]
			// });
			// overlay.display();
		}
	},
	template: function(){
		return templates.pages.pagesectionselectoption();
	}
});