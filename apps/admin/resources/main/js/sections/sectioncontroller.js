/**
 * Created by anders on 29/12/16.
 */
var AmpersandState = require('ampersand-state');
var SectionService = require('./sections');
var PageSectionsItemView = require('./pagesectionsitem');
var PageSectionsView = require('./pagesections');

module.exports = AmpersandState.extend({
	index: function(){
		console.log('pagecontroller.index()');
		var sections = new SectionService();
		var pageSectionsView = new PageSectionsView({
	    	el: document.getElementById('pages-table')
		});
		sections.fetch({
			success: function(collection, response, options){
				pageSectionsView.renderCollection(collection, PageSectionsItemView, '#pages-container');
			}
		});
	}
});