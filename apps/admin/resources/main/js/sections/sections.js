/**
 * Created by anders on 29/12/16.
 */
var Section = require('./section');
var AmpersandRestCollection = require('ampersand-rest-collection');

module.exports = AmpersandRestCollection.extend({
	model: Section,
	mainIndex: 'name',
	url : '/ajax/section'
});
