/**
 * Created by anders on 29/12/16.
 */
var AmpersandView = require('ampersand-view');
var templates = require('../../../../../../build/templates.main.js');

module.exports = AmpersandView.extend({
	bindings : {
		'model.name': {
            type: 'innerHTML',
            selector: '.page-name'
        }
	},
	events: {
		"click .page-option-edit": function(){
			window.location.href = "/main/sections/edit/" + this.model.getId();
		}
	},
	template: function(){
		return  templates.pages.pagelistitem();
	}
});