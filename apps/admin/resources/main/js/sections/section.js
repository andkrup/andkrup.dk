/**
 * Created by anders on 29/12/16.
 */
var AmpersandModel = require('ampersand-model');

module.exports = AmpersandModel.extend({
	props: {
		name: 'string',
		title: 'string',
		editor: 'object'
	},
	urlRoot : '/ajax/section'
});
