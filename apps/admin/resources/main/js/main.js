/**
 * Created by anders on 09/12/16.
 */
/**
 * Created by anders on 20/11/16.
 */

var Router = require('./router');
// var templates = require('../../../../../build/templates.js');
var app = require('ampersand-app');
// var AmpersandView = require('ampersand-view');
var _ = require('lodash');
var domReady = require('domready');

// attach our app to `window` so we can
// easily access it from the console.
window.app = app;

// Extends our main app singleton
app.extend({
    router: new Router(),
	init: function() {
		this.router.history.start();
	}

});

domReady(_.bind(app.init, app));