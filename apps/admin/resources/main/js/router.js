/**
 * Created by anders on 29/12/16.
 */

var PageController = require('./pages/pagecontroller')
var SectionController = require('./sections/sectioncontroller')
var AmpersandRouter = require('ampersand-router');

module.exports = AmpersandRouter.extend({
	controllers: {
	},

	routes:{
		"main" : "index",
		"main/contents" : "contentsIndex",
		"main/pages" : function(){
			var controller = new PageController();
			controller.index();
		},
		"main/pages/:action/:param" : function(action, param){
			var controller = new PageController();
			var fn = controller[action];
			if(typeof fn === 'function'){
				fn.apply(controller, [param]);
			}
		},
		"main/sections" : function(){
			var controller = new SectionController();
			controller.index();
		}
	},

	index : function(){
		console.log('main/');
	},
	contentsIndex : function(){
		console.log('main/contents');
	},
	pagesIndex : function(){
	}
});

