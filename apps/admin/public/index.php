<?php
use Phalcon\Mvc\Application;
// configuration values
$config = include __DIR__ . '/../config/config.php';

// credentials
$auth = include __DIR__ . '/../config/auth.php';

//// handles loading
include __DIR__ . '/../config/loader.php';

// ioc services
include __DIR__ . '/../config/services.php';

// module definitions
$modules = include __DIR__ . '/../config/modules.php';

$application = new Application($di);
$application->registerModules($modules);

try {
    // Handle the request
    $response = $application->handle();
    $response->send();
}
catch (\Exception $e) {
    echo $e->getMessage();
}