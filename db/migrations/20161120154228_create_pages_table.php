<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreatePagesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(){
		if(!$this->hasTable('Pages')){
			$table = $this->table('Pages', ['id' => false, 'primary_key' => 'id']);

			$table->addColumn('id', 'integer', ['signed' => false, 'identity' => true, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('revision', 'integer', ['signed' => false, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('guid', 'string', ['null' => true, 'limit' => 128]);
			$table->addColumn('author', 'integer', ['signed' => false, 'null' => false, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('created', 'datetime', ['null' => false, 'default' => 'CURRENT_TIMESTAMP']);
			$table->addColumn('modified', 'datetime', ['null' => true, 'update' => 'CURRENT_TIMESTAMP']);

			$table->addIndex('guid', ['unique' => true]);

			$table->addForeignKey('author', 'Users', 'id', array('delete'=> 'NO_ACTION', 'update'=> 'CASCADE'));

			$table->create();
		}

    }
}
