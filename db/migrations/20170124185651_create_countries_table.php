<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateCountriesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
	public function change(){
		if(!$this->hasTable('Countries')) {
			$table = $this->table('Countries', ['id' => false, 'primary_key' => 'id']);

			$table->addColumn('id', 'integer', ['signed' => false, 'identity' => true, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('iso3166-1n', 'integer', ['signed' => false, 'null' => true, 'limit' => 4]);
			$table->addColumn('iso3166-1n', 'char', ['null' => false, 'limit' => 2]);
			$table->addColumn('name', 'string', ['null' => false, 'limit' => 127]);
			$table->addColumn('localName', 'string', ['null' => true, 'limit' => 127]);
			$table->addColumn('icc', 'string', ['null' => true, 'limit' => 6]);

			$table->create();
		}
	}
}
