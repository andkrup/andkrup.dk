<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateSectionRevisionsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
	public function change(){
		if(!$this->hasTable('SectionRevisions')) {
			$table = $this->table('SectionRevisions', ['id' => false, 'primary_key' => 'id']);

			$table->addColumn('id', 'integer', ['signed' => false, 'identity' => true, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('user', 'integer', ['signed' => false, 'null' => false, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('objectId', 'string', ['null' => false, 'limit' => 64]);
			$table->addColumn('raw', 'text', ['null' => true]);
			$table->addColumn('editor', 'string', ['null' => true, 'limit' => 128]);
			$table->addColumn('created', 'datetime', ['null' => false, 'default' => 'CURRENT_TIMESTAMP']);

			$table->addForeignKey('user', 'Users', 'id', array('delete'=> 'NO_ACTION', 'update'=> 'NO_ACTION'));

			$table->create();
		}
	}
}
