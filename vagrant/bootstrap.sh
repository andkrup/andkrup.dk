#!/usr/bin/env bash
#
#

# add required repos:
# phalcon repo
curl -s https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | sudo bash
#apt-add-repository ppa:phalcon/stable -y
apt-add-repository ppa:chris-lea/redis-server -y
# mongo repos
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

apt-get update
apt-get upgrade -y

locale-gen en_US.utf8
sed -i '/AcceptEnv LANG LC_*/c\#AcceptEnv LANG LC_*' /etc/ssh/sshd_config

# set mysql root password for scripted mysql install
debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'

apt-get install -y curl vim wget git software-properties-common python-software-properties gettext nodejs npm zip
apt-get install -y apache2 libapache2-mod-fastcgi
apt-get install -y php7.0 php7.0-phalcon php7.0-mysql php7.0-cli php7.0-curl php7.0-gd php7.0-intl php7.0-pgsql php7.0-mbstring php7.0-xml php7.0-xsl php-msgpack php7.0-zip php7.0-fpm
apt-get install -y php-msgpack php-mongodb php-gettext php-redis php-xdebug
apt-get install -y mysql-server mongodb-org redis-server
apt-get autoremove -y

#vagrant        ALL=(ALL)       NOPASSWD: ALL

# phpunit
#wget https://phar.phpunit.de/phpunit.phar -O /usr/local/bin/phpunit
#chown root:root /usr/local/bin/phpunit
#chmod +x /usr/local/bin/phpunit

# fix missing node symlink
ln -s /usr/bin/nodejs /usr/local/bin/node
# install bower, gulp & stylus
npm install -g bower sqwish stylus catchmail ampersand ampersand-collection pug-cli underscore 

# enable apache mods and configs
a2enmod proxy_fcgi setenvif actions rewrite
a2enconf php7.0-fpm

# download & install composer
curl -sS http://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
chown root:root /usr/local/bin/composer
chmod +x /usr/local/bin/composer

# replace /var/log/apache2 with symlink to vagrant folder
rm -rf /var/log/apache2
ln -fsn /vagrant/.apache /var/log/apache2
ln -fsn /vagrant/.xdebug /var/log/xdebug

# pointing enabled default configuration to the one in our repository
ln -fs /vagrant/default.conf /etc/apache2/sites-enabled/000-default.conf
ln -fs /vagrant/admin.conf /etc/apache2/sites-enabled/001-admin.conf
# add our custom php settings
unlink /etc/php/7.0/fpm/php.ini
ln -fs /vagrant/php-fpm.ini /etc/php/7.0/fpm/php.ini

#composer update

# mongod upstart script
ln -fs /vagrant/mongodb.service /etc/systemd/system/mongodb.service
systemctl daemon-reload

service apache2 restart
